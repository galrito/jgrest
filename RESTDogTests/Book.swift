import Foundation
import CoreData
@testable import RESTDog

@objc(Book)
final class Book: NSManagedObject {
  @NSManaged var remoteID: Int32
  @NSManaged var title: String
}

extension Book: RESTEntity {
  static func make(fromJSON json: JSONDictionary, in context: NSManagedObjectContext) -> Book? {
    guard let remoteID = json["id"] as? Int,
      let title = json["title"] as? String else {
        return nil
    }
    
    let newBook = Book(context: context)
    newBook.remoteID = Int32(remoteID)
    newBook.title = title
    return newBook
  }
  
  static var urlEntityName: String {
    return "book"
  }
  
  var json: JSONDictionary {
    return [
      "id": remoteID,
      "title": title
    ]
  }
}
