//
//  JSONDecoderParserTests.swift
//  RESTDogTests
//
//  Created by Jorge Galrito on 09/07/2018.
//  Copyright © 2018 WATERDOG mobile. All rights reserved.
//

import XCTest
import CoreData
@testable import RESTDog

class JSONDecoderParserTests: XCTestCase {
  
  var context: NSManagedObjectContext!
  
  override func setUp() {
    super.setUp()
    let persistentContainer = TestsHelper.makeCoreDataTestingPersistentContainer()
    self.context = persistentContainer.newBackgroundContext()
  }
  
  func testParserCanExist() {
    let parser = DecoderParser(type: Movie.self)
    XCTAssertNotNil(parser)
  }
  
  func testParserWithSingleObject() {
    let json = """
      {
        "remoteIdentifier": 1,
        "title": "Titanic"
      }
    """
    let data = json.data(using: .utf8)!
    let parser = DecoderParser(type: Movie.self)
    parser.parse(jsonData: data, in: context, handler: {_ in})
    
    XCTAssertEqual(context.insertedObjects.count, 1)
  }
  
  func testParserWithArrayOfObjects() {
    let json = """
      [
        {
          "remoteIdentifier": 1,
          "title": "Titanic"
        },
        {
          "remoteIdentifier": 2,
          "title": "Trainspotting"
        }
      ]
    """
    let data = json.data(using: .utf8)!
    let parser = DecoderParser(type: [Movie].self)
    parser.parse(jsonData: data, in: context, handler: {_ in})
    
    XCTAssertEqual(context.insertedObjects.count, 2)
  }
}
