import XCTest
import CoreData
@testable import RESTDog

class ArrayOfEntitesJSONParserTests: XCTestCase {
  var persistentContainer: NSPersistentContainer!
  var context: NSManagedObjectContext!
  var parser: ArrayOfEntitesJSONParser<Book>!
  
  override func setUp() {
    super.setUp()
    persistentContainer = TestsHelper.makeCoreDataTestingPersistentContainer()
    context = persistentContainer.newBackgroundContext()
    parser = ArrayOfEntitesJSONParser<Book>(objectHandler: nil)
  }
  
  func testParserCreatesOneObjectIfGivenOneCorrectItemInJSON() {
    let validBook: JSONDictionary = ["id": 100, "title": "Cosmos"]
    let data = try! JSONSerialization.data(withJSONObject: [validBook], options: [])
    
    parser.parse(jsonData: data, in: context, handler: {_ in })
    
    var fetchRequest: NSFetchRequest<NSNumber>!
    context.performAndWait {
      fetchRequest = Book.fetchRequest() as! NSFetchRequest<NSNumber>
      fetchRequest.includesPendingChanges = false
      fetchRequest.resultType = .countResultType
    }
    
    XCTAssertEqual(try! context.count(for: fetchRequest), 1)
    XCTAssertFalse(context.hasChanges)
  }
  
  func testParserCreatesZeroObjectsIfGivenOneInvalidItemInJSON() {
    let invalidBookJSON = ["no": "book"]
    let data = try! JSONSerialization.data(withJSONObject: invalidBookJSON, options: [])
    parser.parse(jsonData: data, in: context, handler: {_ in})
    
    var fetchRequest: NSFetchRequest<NSNumber>!
    context.performAndWait {
      fetchRequest = Book.fetchRequest() as! NSFetchRequest<NSNumber>
      fetchRequest.resultType = .countResultType
    }
    
    XCTAssertEqual(try! context.count(for: fetchRequest), 0)
  }
  
  func testParserCreatesOnlyObjectForValidBookJSON() {
    let invalidBookJSON: JSONDictionary = ["no": "book"]
    let validBook1: JSONDictionary = ["id": 101, "title": "A World Infested With Demons"]
    let validBook2: JSONDictionary = ["id": 102, "title": "Contact"]
    let data = try! JSONSerialization.data(withJSONObject: [validBook1, invalidBookJSON, validBook2], options: [])
    parser.parse(jsonData: data, in: context, handler: {_ in})
    
    var fetchRequest: NSFetchRequest<NSNumber>!
    context.performAndWait {
      fetchRequest = Book.fetchRequest() as! NSFetchRequest<NSNumber>
      fetchRequest.resultType = .countResultType
    }
    
    XCTAssertEqual(try! context.count(for: fetchRequest), 2)
  }
}
