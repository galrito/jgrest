//
//  Movie.swift
//  RESTDogTests
//
//  Created by Jorge Galrito on 10/07/2018.
//  Copyright © 2018 WATERDOG mobile. All rights reserved.
//

import Foundation
import CoreData
@testable import RESTDog

@objc(Movie)
class Movie: NSManagedObject, RemoteEntityConvertible {
  @NSManaged var remoteIdentifier: Int32
  @NSManaged var title: String
  
  required init(from decoder: Decoder) throws {
    guard let context = decoder.userInfo[.managedObjectContext] as? NSManagedObjectContext else {
      fatalError()
    }
    
    super.init(entity: Movie.entity(), insertInto: context)
    
    let container = try decoder.container(keyedBy: CodingKeys.self)
    remoteIdentifier = try container.decode(Int32.self, forKey: .remoteIdentifier)
    title = try container.decode(String.self, forKey: .title)
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(remoteIdentifier, forKey: .remoteIdentifier)
    try container.encode(title, forKey: .title)
  }
  
  private enum CodingKeys: String, CodingKey {
    case remoteIdentifier
    case title
  }
}
