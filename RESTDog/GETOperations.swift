import Foundation
import CoreData
import WDOperations

open class GetAllEntitiesRESTOperation<T: RESTRemoteService>: WDOperation where T.Entity: NSManagedObject {
  let internalQueue: WDOperationQueue
  let remoteService: T
  let context: NSManagedObjectContext
  let parser: JSONParserHandler
  
  public var objectHandler: ((T.Entity) -> Void)?
  
  public var fetchSubsequentPage = false
  
  private let pageNumberToFetch: Int?
  private var completionOperation: Operation!
  
  public init(remoteService: T, pageNumber: Int? = nil, context: NSManagedObjectContext, parser: JSONParserHandler, conditions: [WDOperationCondition]) {
    self.internalQueue = WDOperationQueue()
    internalQueue.isSuspended = true
    
    self.pageNumberToFetch = pageNumber
    self.remoteService = remoteService
    self.context = context
    self.parser = parser
    
    super.init()
    
    conditions.forEach { self.addCondition($0) }
    
    self.completionOperation = BlockOperation {
      self.finish()
    }
  }
  
  override open func execute() {
    if !isCancelled {
      let uuid = UUID().uuidString
      var cacheURL = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
      cacheURL.appendPathComponent(uuid)
      
      let request = remoteService.getAll(page: pageNumberToFetch)
      let task = URLSessionTaskOperation(request: request, cache: cacheURL, completion: { [parser] response in
        if self.fetchSubsequentPage,
          let response = response,
          let nextPage = self.remoteService.getNextPageNumber(from: response) {
            print(nextPage)
            let nextPageOperation = GetAllEntitiesRESTOperation(
              remoteService: self.remoteService,
              pageNumber: nextPage,
              context: self.context,
              parser: parser,
              conditions: self.conditions
            )
            nextPageOperation.fetchSubsequentPage = true
            self.completionOperation.addDependency(nextPageOperation)
            self.internalQueue.addOperation(nextPageOperation)
        }
      })
      internalQueue.addOperation(task)
      
      let parseOperation = ParseJSONOperation(cache: cacheURL, context: context, parser: parser, handler: {_ in })
      parseOperation.addDependency(task)
      internalQueue.addOperation(parseOperation)
      
      completionOperation.addDependency(parseOperation)
      internalQueue.addOperation(completionOperation)
      
      internalQueue.isSuspended = false
    }
  }
  
  override open func cancel() {
    internalQueue.cancelAllOperations()
    super.cancel()
  }
  
}

open class GetEntityRESTOperation<T: RESTRemoteService>: WDOperation where T.Entity: NSManagedObject {
  let internalQueue: WDOperationQueue
  let remoteID: Int
  let remoteService: T
  
  public init(remoteID: Int, remoteService: T, conditions: [WDOperationCondition]) {
    self.internalQueue = WDOperationQueue()
    internalQueue.isSuspended = true
    
    self.remoteService = remoteService
    self.remoteID = remoteID
    
    super.init()
    
    conditions.forEach { self.addCondition($0) }
  }
  
  override open func execute() {
    if !isCancelled {
      let uuid = UUID().uuidString
      var cacheURL = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
      cacheURL.appendPathComponent(uuid)
      
      let request = remoteService.getEntity(withRemoteID: remoteID)
      let task = URLSessionTaskOperation(request: request, cache: cacheURL)
      internalQueue.addOperation(task)
      
      internalQueue.isSuspended = false
    }
  }
  
  override open func cancel() {
    internalQueue.cancelAllOperations()
    super.cancel()
  }
}
