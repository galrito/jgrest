//
//  DecoderParser.swift
//  RESTDog
//
//  Created by Jorge Galrito on 08/07/2018.
//  Copyright © 2018 WATERDOG mobile. All rights reserved.
//

import Foundation
import CoreData

public extension CodingUserInfoKey {
  public static let managedObjectContext = CodingUserInfoKey(rawValue: "mobi.waterdog.RESTDog.managedObjectContextKey")!
}

public struct DecoderParser<T: CoreDataCodable & Decodable>: JSONParserHandler {
  
  public init(type: T.Type) {
  }
  
  public func parse(jsonData: Data, in context: NSManagedObjectContext, handler: @escaping (Bool) -> Void) {
    var success = false
    defer {
      handler(success)
    }
    
    context.performAndWait {
      do {
        let decoder = JSONDecoder()
        decoder.userInfo[.managedObjectContext] = context
        _ = try decoder.decode(T.self, from: jsonData)
        success = true
      } catch {
      }
    }
  }
}
