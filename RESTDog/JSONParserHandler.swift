import Foundation
import CoreData

public protocol JSONParserHandler {
  func parse(jsonData: Data, in context: NSManagedObjectContext, handler: @escaping (Bool) -> Void)
}

public struct ArrayOfEntitesJSONParser<T: RESTEntity>: JSONParserHandler where T: NSManagedObject {
  private let objectHandler: ((T) -> Void)?
  
  public init(objectHandler: ((T) -> Void)?) {
    self.objectHandler = objectHandler
  }
  
  public func parse(jsonData: Data, in context: NSManagedObjectContext, handler: @escaping (Bool) -> Void) {
    var success = false
    defer {
      handler(success)
    }
    
    do {
      if let jsonArray = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [JSONDictionary] {
        parse(jsonObject: jsonArray, in: context)
        success = true
      }
    } catch {
    }
  }
  
  public func parse(jsonObject: [JSONDictionary], in context: NSManagedObjectContext) {
    context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    context.performAndWait {
      jsonObject.forEach { dict in
        if let entity = T.make(fromJSON: dict, in: context) {
          self.objectHandler?(entity)
        }
      }
      
      do {
        try context.save()
      } catch {
        print(error)
      }
    }
  }
}

public struct SingleEntityJSONParser<T: RESTEntity>: JSONParserHandler where T: NSManagedObject {
  let completionHandler: (Bool) -> Void
  let objectHandler: ((T) -> Void)?
  var entityToUpdate: T?
  
  public init(updateEntity: T? = nil, objectHandler: ((T) -> Void)? = nil, completionHandler: @escaping (Bool) -> Void) {
    self.entityToUpdate = updateEntity
    self.completionHandler = completionHandler
    self.objectHandler = objectHandler
  }
  
  public func parse(jsonData: Data, in context: NSManagedObjectContext, handler: @escaping (Bool) -> Void) {
    var success = false
    defer {
      handler(success)
    }
    
    do {
      if let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: []) as? JSONDictionary {
        parse(jsonObject: jsonObject, in: context)
        success = true
      }
    } catch {
    }
  }
  
  public func parse(jsonObject: JSONDictionary, in context: NSManagedObjectContext) {
    context.performAndWait {
      var success = false
      guard let remoteID = jsonObject["id"] as? Int else {
        return
      }
      
      if var entityToUpdate = self.entityToUpdate {
        entityToUpdate.remoteID = Int32(remoteID)
      }
      
      let newEntity = T.make(fromJSON: jsonObject, in: context)
      if let entity = newEntity {
        self.objectHandler?(entity)
      }
      success = (newEntity != nil)
      
      try! context.save()
      self.completionHandler(success)
    }
  }
}
